def syracuse(n):
    if n%2==0:
        return n/2
    else:
        return 3*n+1


# Tests
assert syracuse(10)==5
assert syracuse(5)==16

