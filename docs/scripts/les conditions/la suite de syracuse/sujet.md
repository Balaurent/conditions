---
author: BL
title: suite de Syracuse
tags:
    - boucle
---

Programmer la fonction `syracuse`, prenant en paramètre un entier `n`, et qui renvoie `n/2` si `n` est pair et `3n+1` si `n` est impair.

!!! example "Exemples"

    ```pycon
    >>> syracuse(10)
    5
    >>> syracuse(5)
    16
    ```

{{ IDEv('exo') }}
