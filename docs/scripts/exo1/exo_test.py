# Tests
assert syracuse(10) == 5
assert syracuse(5) == 16
# Tests supplémentaires
assert syracuse(11) == 37
assert syracuse(2) == 1
assert syracuse(9) == 28
assert syracuse(18) == 9
